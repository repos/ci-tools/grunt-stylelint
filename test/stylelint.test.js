'use strict';

const grunt = require( 'grunt' );
const path = require( 'path' );

exports.stylelint = {
	outputFile: function ( test ) {
		const actual = grunt.file.readJSON( 'tmp/outputFile/report.json' );
		const expected = grunt.file.readJSON( 'test/expected/outputFile/report.json' );

		actual.forEach( ( file ) => {
			// Normalize path
			file.source = path.relative( process.cwd(), file.source );
		} );

		test.deepEqual( actual, expected, 'Should report to file.' );
		test.done();
	}
};
